<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of all open and close times</h1>
        <ul>
            <?php
		$json = file_get_contents('http://laptop-service:5000/listAll');
            	$obj = json_decode($json);
	        $times = $obj->time;
            	foreach ($times as $t) {
		  echo "<li>$t</li>";
		}
		?>
	</ul>

	<h1>List of all opening times</h1>
	<ul>
	<?php
		$json = file_get_contents('http://laptop-service:5000/listOpenOnly');
            	$obj = json_decode($json);
	        $times = $obj->time;
            	foreach ($times as $t) {
		  echo "<li>$t</li>";
            }
            ?>
	</ul>

	<h1>List of all closing times</h1>
	<ul>
	<?php
		$json = file_get_contents('http://laptop-service:5000/listCloseOnly');
            	$obj = json_decode($json);
	        $times = $obj->time;
            	foreach ($times as $t) {
		  echo "<li>$t</li>";
            }
            ?>
	</ul>

	<h1>List of all opening and close times in CSV</h1>
	<ul>
	<?php	
		$csv = file_get_contents('http://laptop-service:5000/listAll/csv');
		$times = str_getcsv($csv);
		foreach ($times as $t){
		    echo "$t";
		}
            ?>
	</ul>

	<h1>List of all opening times in CSV</h1>
	<ul>
	<?php	
		$csv = file_get_contents('http://laptop-service:5000/listCloseOnly/csv');
		$times = str_getcsv($csv);
		foreach ($times as $t){
		    echo "$t";
		}
            ?>
	</ul>

	<h1>List of all closing times in CSV</h1>
	<ul>
	<?php	
		$csv = file_get_contents('http://laptop-service:5000/listCloseOnly/csv');
		$times = str_getcsv($csv);
		foreach ($times as $t){
		    echo "$t";
		}
            ?>
        </ul>

        <h1>List of all open and close times in JSON</h1>
        <ul>
            <?php
		$json = file_get_contents('http://laptop-service:5000/listAll/json');
            	$obj = json_decode($json);
	        $times = $obj->time;
            	foreach ($times as $t) {
		  echo "<li>$t</li>";
		}
		?>
	</ul>

	<h1>List of all opening times in JSON</h1>
	<ul>
	<?php
		$json = file_get_contents('http://laptop-service:5000/listOpenOnly/json');
            	$obj = json_decode($json);
	        $times = $obj->time;
            	foreach ($times as $t) {
		  echo "<li>$t</li>";
            }
            ?>
	</ul>

	<h1>List of all closing times in JSON</h1>
	<ul>
	<?php
		$json = file_get_contents('http://laptop-service:5000/listCloseOnly/json');
            	$obj = json_decode($json);
	        $times = $obj->time;
            	foreach ($times as $t) {
		  echo "<li>$t</li>";
            }
            ?>
	</ul>

	<h1>List of first 4 opening times in CSV</h1>
	<ul>
	<?php	
		$csv = file_get_contents('http://laptop-service:5000/listOpenOnly/csv?top=4');
		$times = str_getcsv($csv);
		foreach ($times as $t){
		    echo "$t";
		}
            ?>
        </ul>
	<h1>List of first 3 opening times in JSON</h1>
	<ul>
	<?php
		$json = file_get_contents('http://laptop-service:5000/listOpenOnly/json?top=3');
            	$obj = json_decode($json);
	        $times = $obj->time;
            	foreach ($times as $t) {
		  echo "<li>$t</li>";
            }
            ?>
	</ul>

	<h1>List of first 6 closing times in CSV</h1>
	<ul>
	<?php	
		$csv = file_get_contents('http://laptop-service:5000/listCloseOnly/csv?top=6');
		$times = str_getcsv($csv);
		foreach ($times as $t){
		    echo "$t";
		}
            ?>
        </ul>

	<h1>List of first 5 close times JSON</h1>
	<ul>
	<?php
		$json = file_get_contents('http://laptop-service:5000/listCloseOnly/json?top=5');
            	$obj = json_decode($json);
	        $times = $obj->time;
            	foreach ($times as $t) {
		  echo "<li>$t</li>";
            }
            ?>
	</ul>

    </body>
</html>
