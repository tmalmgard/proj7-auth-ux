"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
from flask import request, render_template, flash, redirect, url_for
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from flask_restful import Resource, Api
from forms import LoginForm, RegisterForm
import logging
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time


app = flask.Flask(__name__)
api = Api(app)
client = MongoClient("172.18.0.2", 27017)

db = client.tododb
db2 = client.passdb
CONFIG = config.configuration()
app.secret_key = "SECRET KEY"

db_array=[]

current_users = {}

login_manager = LoginManager()

login_manager.setup_app(app)

login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active
        self.token = None
        self.authenticated = False

    def is_active(self):
        return self.active

    def is_authenticated(self):
        return self.authenticated
    
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def get_username(self):
        return self.name

def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)



@login_manager.user_loader
def load_user(id):
    if id in current_users:
        return current_users[id]
    else:
        return None
     

def generate_auth_token(user, expiration=600):
   s = Serializer(app.secret_key, expires_in=expiration)
   # pass index of user
   id = int(user.get_id())
   return s.dumps({'ID': id})

def verify_auth_token(token):
    s = Serializer(app.secret_key)
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return True

###
# Pages
###


class listAll(Resource):
    @login_required
    def get(self):
        if(verify_auth_token(current_user.token)):
            _items = db.tododb.find()
            items = [item for item in _items]
            timeItems = []
            for item in items:
                timeItems.append(item['open'] + " " + item['close'])
            return {"time":timeItems}
        else:
            return "Token not valid or expired", 401



class listAllJSON(Resource):
    @login_required
    def get(self):
        if(verify_auth_token(current_user.token)):
            _items = db.tododb.find()
            items = [item for item in _items]
            timeItems=[]
            for item in items:
                timeItems.append(item['open'] + " " + item['close'])
            return {"time":timeItems}
        else:
            return "Token not valid or expired", 401

class listOpenOnly(Resource):
    @login_required
    def get(self):
        if(verify_auth_token(current_user.token)):
            _items = db.tododb.find()
            app.logger.debug("From database: {}".format(_items))
            items = [item for item in _items]
            timeItems = []
            for item in items:
                timeItems.append(item['open'])
            return {"time":timeItems}
        else:
            return "Token not valid or expired", 401

class listOpenOnlyJSON(Resource):
    @login_required
    def get(self):
        if(verify_auth_token(current_user.token)):
            _items = db.tododb.find()
            items = [item for item in _items]
            timeItems = []
            top = flask.request.args.get('top')
            if (top == None) or (int(top)>len(items)):
                top = len(items)
            for i in range(int(top)):
                timeItems.append(items[i]['open'])
            return {"time":timeItems}
        else:
            return "Token not valid or expired", 401
        

class listCloseOnly(Resource):
    @login_required
    def get(self):
        if(verify_auth_token(current_user.token)):
            _items = db.tododb.find()
            app.logger.debug("From database: {}".format(_items))
            items = [item for item in _items]
            timeItems = []
            for item in items:
                timeItems.append(item['close'])
            return {"time":timeItems}
        else:
            return "Token not valid or expired", 401

class listCloseOnlyJSON(Resource):
    @login_required
    def get(self):
        if(verify_auth_token(current_user.token)):
            _items = db.tododb.find()
            items = [item for item in _items]
            timeItems = []
            top = flask.request.args.get('top')
            if (top == None) or (int(top)>len(items)):
                top = len(items)

            for i in range(int(top)):
                timeItems.append(items[i]['close'])
            return {"time":timeItems}
        else:
            return "Token not valid or expired", 401


class listOpenOnlyCSV(Resource):
    @login_required
    def get(self):
        if(verify_auth_token(current_user.token)):
            _items = db.tododb.find()
            app.logger.debug("From database: {}".format(_items))
            items = [item for item in _items]
            itemStr = ""
            top = flask.request.args.get('top')
            if (top == None) or (int(top)>len(items)):
                top = len(items)
            for i in range(int(top)):
                itemStr = itemStr + str(items[i]['open']) + ", "
            return itemStr
        else:
            return "Token not valid or expired", 401


class listCloseOnlyCSV(Resource):
    @login_required
    def get(self):
        if(verify_auth_token(current_user.token)):
            _items = db.tododb.find()
            app.logger.debug("From database: {}".format(_items))
            items = [item for item in _items]
            itemStr = ""
            top = flask.request.args.get('top')
            if (top == None) or (int(top)>len(items)):
                top = len(items)
            for i in range(int(top)):
                itemStr = itemStr + str(items[i]['close']) + ", "
            return itemStr
        else:
            return "Token not valid or expired", 401


class listAllCSV(Resource):
    @login_required
    def get(self):
        app.logger.debug("current user is authenticated in API: {}".format(current_user.is_authenticated))
        if(verify_auth_token(current_user.token)):
            _items = db.tododb.find()
            app.logger.debug("From database: {}".format(_items))
            items = [item for item in _items]
            itemStr = ""
            for item in items:
                itemStr = itemStr + str(item['open'])+ ", " + str(item['close']) + ", "
            return itemStr
        else:
            return "Token not valid or expired", 401

class registerAPI(Resource):
    def get(self):
        Username = flask.request.args.get('username')
        Password = hash_password(flask.request.args.get('password'))
        ID = str(db.passdb.count({})+1)
        User = {"ID":ID, "Username":Username, "Password":Password}
        app.logger.debug("Username: {}".format(Username))
        _users=db.passdb.find({"Username": Username})
        users = [user for user in _users]
        if(len(users)==0): 
            db.passdb.insert_one(User)
            del User['_id']
            response = app.response_class(
                    response=flask.json.dumps(User), 
                    status = 201
            )
            return response
        else:
            return ("Username already taken, try again", 400)

class token(Resource):
    @login_required
    def get(self):
        app.logger.debug("In token generation: Username: {} ID: {}".format(current_user.get_username, current_user.get_id()))
        current_user.token = generate_auth_token(current_user, 20)
        return "Success"



api.add_resource(token, '/token')
api.add_resource(registerAPI, '/api/register')
api.add_resource(listAll, '/listAll')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listAllJSON, '/listAll/json')
api.add_resource(listOpenOnlyJSON, '/listOpenOnly/json')
api.add_resource(listCloseOnlyJSON, '/listCloseOnly/json')
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')

@app.route("/")
@app.route("/index")
@login_required
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    app.logger.debug('entered login')
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        user=db.passdb.find_one({"Username": form.username.data})
        if(user):
            if(verify_password(form.password.data, user['Password'])):
                user_obj =User(user['Username'], user['ID'])
                user_obj.authenticated = True
                id = user_obj.get_id()
                remember = request.form.get("remember", "no") == "yes"
                current_users[id]=user_obj
                if(login_user(user_obj, remember = remember, force = True)):
                    name = user_obj.get_username()
                    app.logger.debug("User_object: Name: {} ID: {} ID type: {}, current user is authenticated: {}".format(name, id, type(id), current_user.is_authenticated()))
            
                    current_user.token = generate_auth_token(current_user, 10)
                    flash("Logged in!")
                    next = flask.request.args.get("next")

                return redirect(next or url_for("index"))
        
            else:
                flash("Wrong password")
        else:
            flash("Wrong username")
    return render_template('login.html', title='Sign In', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    app.logger.debug('entered register')
    form = RegisterForm()
    if form.validate_on_submit():
        flash('Registered user {}'.format(
            form.username.data))
        user=db.passdb.find_one({"Username": form.username.data})
        if(user):
            flash("Username taken")
        else:
            url = "/api/register?username="+ form.username.data + "&password="+form.password.data
            return redirect(url)
    return render_template('register.html', title='Register', form=form)

@app.route('/sub')
def sub():
    app.logger.debug("Entered submit function")
    app.logger.debug("db_array: {}".format(db_array))
    for item in db_array:
        db.tododb.insert_one(item)
    return "nothing"

@app.route("/disp")
def display():
    app.logger.debug("Entered display function")
    _items = db.tododb.find()
    app.logger.debug("From database: {}".format(_items))
    items = [item for item in _items]
    app.logger.debug("Printing items: {}".format(items))
    return flask.render_template('todo.html', items=items)
 
@app.route("/logout")
@login_required
def logout():
    logout_user()

    flash("Logged out.")
    return redirect(url_for("index"))

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
#@login_required
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', 200, type=float)
    beginDate = request.args.get('begindate', str)
    beginTime = request.args.get('begintime', str)
    beginDate = beginDate + " " + beginTime
    app.logger.debug("km={}".format(km))
    app.logger.debug("dist={}".format(distance))
    app.logger.debug("time={}".format(beginDate))

    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, beginDate)
    close_time = acp_times.close_time(km, distance, beginDate)
    item_dict = {"km": km, "distance": distance, "beginDate": beginDate, "open":open_time, "close": close_time  }
    app.logger.debug("item_dict: {}".format(item_dict))
    db_array.append(item_dict)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(host="0.0.0.0", debug=True)
